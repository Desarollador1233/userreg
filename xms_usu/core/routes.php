<?php 

    function loadController($controlador){
        $nameController=$controlador."Controller";
        $fileController='controller/'.$controlador.'.php';

        if(!is_file($fileController)){
            $fileController='controller/'.MAIN_CONTROLLER.'.php';
        }
        require_once $fileController;
            $control=new $nameController;
            return $control;

    }

    function loadAction($controller, $accion){
		
		if(isset($accion) && method_exists($controller, $accion)){
                
                
				$controller->$accion();
			
			} else {
            
            print_r($controller);
            $controller->$accion();
			
		}	
	}

    


?>